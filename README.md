# poectl

Control your PoE.


## Usage

    poectl <command> [<port>] ...

    where <command> is one of

        enable|disable|toggle       enable, disable, or toggle power to a port
        low|med|high                set power priority of port
        status                      show system status; if <port> is specified, show port status

### Examples

  - set port `swp1` to low priority

        $ poectl low swp1

  - enable power on all ports

        $ poectl enable

  - show status of ports `swp1` to `swp4`

    ```
    $ poectl status swp1-swp4
    Port    Status         Allocated    Priority  PD type      PD class   Voltage   Current    Power
    -----   -----------   -----------   -------- -----------   --------   -------   -------   ---------
    swp1    connected     negotiating   low      IEEE802.3at   4          53.5 V     25 mA    3.9 W
    swp2    searching     none          low      IEEE802.3at   none        0.0 V      0 mA    0.0 W
    swp3    connected     none          low      IEEE802.3at   2          53.5 V     25 mA    1.4 W
    swp4    connected     51.0 W        low      IEEE802.3at   4          53.6 V     72 mA    3.8 W
    ```

  - show system status

    ```
    $ poectl -s
    System power:
      Total:      730.0 W
      Used:        11.0 W
      Available:  719.0 W
    Connected ports:
      swp11, swp24, swp27, swp48
    ```


## Config

A config file at `/etc/poectl.yaml` can be used to setup port configuration.


```yaml
ports:
  - name: swp1
    enabled: true # default
    priority: mid # default
  - name: swp2
    priority: high
  - name: swp3
    enabled: false
    priority: low
```


## Definitions

**Status** can be one of the following:

  - **disabled**: The port has been disabled.
  - **searching**: No device has been detected.
  - **connected**: A device is connected and receiving power.
  - **denied**: There is insufficient power available for the device.

**Allocated** defines how much power has been allocated to the port:

  - **none**: No device is connected or the device does not support LLDP negotiation.
  - **negotiating**: A device is connected and is negotiating for power.
  - **XX.X W**: A device has negotiated for XX.X watts of power.


## Supported Devices

 - bcm59121
