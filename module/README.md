# poe

    make


https://biot.com/blog/posts/tackling-power-over-ethernet/

https://forum.openwrt.org/t/support-for-rtl838x-based-managed-switches/57875/522


## AS4610-54P

 - **STM32F030C8T6**; controller
 - **BCM59121**; PSE (power-sourcing equipment) (x7)
 - 

The SoC communicates to the controller for managing the PSEs.

To install

    ./build.sh
    ssh switch mkdir /lib/modules/$(uname -r)/poe
    scp *.ko switch:'/lib/modules/$(uname -r)/poe/'
    ssh switch
    depmod -a
    modprobe poe_bcm59121

Enable auto-mode

    echo $PIN > /sys/class/gpio/export
    echo out > /sys/class/gpio$PIN/direction
    echo 0/1? > /sys/class/gpio$PIN/value


### packages

  - lldpd

    $ lldpcli show neighbors

### LED

  PoE:
   - green: system has power budget for PoE
   - amber: system doesn't have power budget
  RJ45:

|      | green | green blinking | amber | amber blinking | off
|------|-------|-----------|------|--------|-----|
|  poe | sufficient power | | insufficient power | | |
| rj45 | link w/o poe | activity w/o poe | link w/ poe | activity w/ poe | no link
