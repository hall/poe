set -euxo pipefail

make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi-
scp *.ko switch:'/lib/modules/$(uname -r)/poe/'

